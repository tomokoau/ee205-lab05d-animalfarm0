#include "catDatabase.h"


extern char arr_name[MAXIMUM_CAT][MAX_CAT_NAME];
extern int arr_gender[MAXIMUM_CAT];
extern int arr_breed[MAXIMUM_CAT];
extern bool arr_is_fixed[MAXIMUM_CAT];
extern float arr_weight[MAXIMUM_CAT];
extern int NUM_CATS;
int updateCatName(int index, char* newName){
   //   printf ("hello cat im here\n");
   if (strlen(newName) == 0 || strlen(newName) > MAX_CAT_NAME){
      printf("error: name must be of length 1 to %d\n",MAX_CAT_NAME);
      return -1;
   }
   //printf("im still here -_-\n");
   for(int i=0; i<NUM_CATS;i++){
      if ( strcmp(arr_name[i], newName)==0){
         printf("error: new cat name already exists\n");
         return -1;
      }
   }
   //printf("why did you skip over me\n");
   strcpy(arr_name[index],newName);
   return 0;
}
int fixCat(int index){
   arr_is_fixed[index]=true;
   return 0;
}
int updateCatWeight(int index, float newWeight){

   if (newWeight <= 0.0){
    
      printf("error: is your cat %s antimatter? weight = %f\n", arr_name[index], newWeight);
      return -1;
   }
   arr_weight[index]= newWeight;
   return 0;
}

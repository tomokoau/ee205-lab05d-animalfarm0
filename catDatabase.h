#ifndef GLOBAL_H

#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#define GLOBAL_H
#define MAX_CAT_NAME 30
#define MAXIMUM_CAT 10

#endif

enum gender{UNKNOWN_GENDER, MALE, FEMALE};
enum breed {UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHINX};

#include "catDatabase.h"

extern char arr_name[MAXIMUM_CAT][MAX_CAT_NAME];
extern int arr_gender[MAXIMUM_CAT];
extern int arr_breed[MAXIMUM_CAT];
extern bool arr_is_fixed[MAXIMUM_CAT];
extern float arr_weight[MAXIMUM_CAT];
extern int NUM_CATS;

int addCat(char *, int, int, bool, float);
int printCat(int);
int printAllCats();
int findCat(char *);
int updateCatName(int, char *);
int fixCat(int);
int updateCatWeight(int, float);
int deleteAllCats();

int main (){
   addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
   printAllCats();
   
   int kali = findCat( "Kali" ) ;
  updateCatName( kali, "Chili" ) ; // this should fail
   printCat( kali );
   updateCatName( kali, "Capulet" ) ;
   updateCatWeight( kali, 9.9 ) ;
   fixCat( kali ) ;
   printCat( kali );
   printAllCats();
   deleteAllCats();
   printAllCats();
   return 0;
}

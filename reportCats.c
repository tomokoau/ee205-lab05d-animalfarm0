#include "catDatabase.h"

extern char arr_name[MAXIMUM_CAT][MAX_CAT_NAME];
extern int arr_gender[MAXIMUM_CAT];
extern int arr_breed[MAXIMUM_CAT];
extern bool arr_is_fixed[MAXIMUM_CAT];
extern float arr_weight[MAXIMUM_CAT];
extern int NUM_CATS;

int printCat(long unsigned int index) {
   if (index>=0&& index<NUM_CATS) {
      printf("cat index = [%lu] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n",index,arr_name[index],arr_gender[index],arr_breed[index],arr_is_fixed[index],arr_weight[index]);
      return 0;
   }
   else {
      printf("animalFarm0: Bad cat [%lu]\n",index);
   }
   return -1;
}


int printAllCats(){
   for (int i=0; i<NUM_CATS; i++){
      printCat(i);
   }
   return 0;
}

int findCat(char cat_name[]){
   for(int i=0; i<NUM_CATS;i++){
      if ( strcmp(arr_name[i], cat_name)==0){
         return i;
      }
   }
   
   printf("cat not found\n");
   
   return -1;
}

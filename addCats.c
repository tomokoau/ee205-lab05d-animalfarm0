#include "catDatabase.h"

extern char arr_name[MAXIMUM_CAT][MAX_CAT_NAME];
extern int arr_gender[MAXIMUM_CAT];
extern int arr_breed[MAXIMUM_CAT];
extern bool arr_is_fixed[MAXIMUM_CAT];
extern float arr_weight[MAXIMUM_CAT];
extern int NUM_CATS;

int addCat(char cat_name[],int gender,int breed, bool is_fixed, float weight){
   
   if (NUM_CATS == MAXIMUM_CAT){
      printf("error: database is full(max size is %d)\n", MAXIMUM_CAT);
      return -1;
   }

   if (strlen(cat_name) == 0 || strlen(cat_name) > MAX_CAT_NAME){
      printf("error: name must be of length 1 to %d\n",MAX_CAT_NAME);
      return -1;
   }
   
   for(int i=0; i<NUM_CATS;i++){
      if ( strcmp(arr_name[i], cat_name)==0){
         printf("error: cat name already exists\n");
         return -1;
      }
   }

   if (weight <= 0.0){
    
      printf("error: is your cat %s antimatter? weight = %f\n", cat_name, weight);
      return -1;
   }
   
   strcpy(arr_name[NUM_CATS], cat_name);
   arr_gender[NUM_CATS]= gender;
   arr_breed[NUM_CATS]=breed;
   arr_is_fixed[NUM_CATS]=is_fixed;
   arr_weight[NUM_CATS]=weight;
   NUM_CATS++;
   return 0;
}



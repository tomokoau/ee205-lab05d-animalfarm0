all: main.c reportCats.c addCats.c catDatabase.c catDatabase.h updateCats.c deleteCats.c
	gcc -Wall -pedantic-errors main.c reportCats.c addCats.c catDatabase.c deleteCats.c updateCats.c

clean:
	rm a.out
